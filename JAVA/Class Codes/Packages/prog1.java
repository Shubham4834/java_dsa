
import java.util.Scanner;
import arithfun.ArithmeticOpertion;

class Client{
	
	public static void main (String [] Args){
		
		Scanner sc = new Scanner(System.in);

		int num1 = sc.nextInt();
		int num2 = sc.nextInt();

		ArithmeticOpertion obj= new ArithmeticOpertion(num1,num2);
		System.out.println(obj.Add());
		System.out.println(obj.Sub());
	}
}
