	// Creating child thread using Runable Interface
	

class MyThread implements Runnable{
	
	public void run (){
		
		System.out.println("In MyThread");
		
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo{
	
	public static void main ( String [] Args){
		
		MyThread obj = new MyThread();
		Thread obj1 = new Thread(obj);
		obj1.start();

		System.out.println(Thread.currentThread().getName());
	}
}
