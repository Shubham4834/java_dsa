
class MyThread extends Thread {
	
	MyThread ( ThreadGroup TG , String str){
	
		super (TG , str); 
	}

	public void run(){
		
		System.out.println(Thread.currentThread());

		try {
			Thread.sleep(50000);
		} catch ( InterruptedException ie ){}
	}
}

class Client{

	public static void main (String [] Args){
		
		ThreadGroup pThreadGp = new ThreadGroup ("India");

		MyThread t1 = new MyThread (pThreadGp , "Maha");
		MyThread t2 = new MyThread (pThreadGp , "Goa");
		t1.start();
		t2.start();

		ThreadGroup cThreadGp = new ThreadGroup (pThreadGp , "Pakistan");

		MyThread t3 = new MyThread (cThreadGp , "Karachi");
		MyThread t4 = new MyThread (cThreadGp , "Lahore");
		
		t3.start();
		t4.start();

		ThreadGroup cThreadGp1 = new ThreadGroup ( pThreadGp , "BanglaDesh");


		MyThread t5 = new MyThread( cThreadGp1 , "Dakha");
		MyThread t6 = new MyThread( cThreadGp1 , "Mizorma");

		t5.start();
		t6.start();
	}
}
