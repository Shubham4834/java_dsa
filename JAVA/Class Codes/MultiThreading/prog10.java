
class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	MyThread(){
		super();
	}

	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
class ThreadGroupDemo{
	public static void main(String[]args){
		MyThread obj = new MyThread("XYZ");
		obj.start();
		MyThread obj1= new MyThread("ABC");
		obj1.start();
		MyThread obj2= new MyThread();
		obj2.start();
	}
}

