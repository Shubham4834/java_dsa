	// Priority of Threads
	
class MyThread extends Thread{
	
	public void run(){
		
		Thread T = Thread.currentThread();
		System.out.println(T.getName());
		System.out.println(T.getPriority());
	}
}

class ThreadDemo{
	
	public static void main (String [] Args){
		
		Thread T = Thread.currentThread();

		System.out.println(T.getName());
		System.out.println(T.getPriority());

		MyThread obj = new MyThread();
		obj.start();
		
		T.setPriority(7);// Range betn 1 to 10

		MyThread obj2 = new MyThread();
		obj2.start();

		System.out.println(T.getPriority());// Priority of main method is set to Override Method
	}
}
