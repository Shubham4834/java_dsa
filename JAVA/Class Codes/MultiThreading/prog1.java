
	// creating Thread by Using ThreadClass
	

class MyThread extends Thread {
	
	public void Run () {

		System.out.println(Thread.currentThread().getName());

		for ( int i=0 ; i<5 ; i++){
			
			System.out.println("In MyThread");
			try {
				Thread.sleep(1000);
			} catch ( InterruptedException obj ){}
		}
	}
}

class ThreadDemo{
	
	public static void main ( String [] Args)throws InterruptedException{


			System.out.println(Thread.currentThread().getName());

		
		MyThread obj = new MyThread();
		obj.start();

		for (int i=0; i<5 ; i++){
			
			System.out.println("In Main");
			Thread.sleep (1000);
		}
	}
}
