
	// Join Method DeadLock condition
	
class MyThread extends Thread{

	static Thread obj1 = null;
	
	public void run(){

		try{
			obj1.join();
		} catch (InterruptedException obj2){}

		
		for ( int i=0 ; i<10 ; i++){
			System.out.println("In thread 0");
		}
	}
}

class ThreadDemo{
	
	public static void main (String [] Args){

		MyThread.obj1 = Thread.currentThread();
		
		MyThread obj = new MyThread();
		obj.start();

		try {
			obj.join();
		} catch ( InterruptedException obj1){}

		for ( int i =0 ; i<10 ; i++){
			
			System.out.println("In Main");
		}
	}
}
