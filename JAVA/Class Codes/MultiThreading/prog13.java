

class MyThread extends Thread {
	
	MyThread ( ThreadGroup TG , String str){
		
		super(TG , str)	;
	}

	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo{
	
	public static void main (String [] Args){

		ThreadGroup pThreadGP = new ThreadGroup ("Core2Web");
		
		MyThread obj = new MyThread( pThreadGP , "Java");
		obj.start();

		MyThread obj1 = new MyThread( pThreadGP , "DSA");
		obj1.start();

		MyThread obj2 = new MyThread( pThreadGP , "C");
		obj2.start();
	}
}
