

	interface Demo{
		
		static void fun(){
			
			System.out.println("In Fun ");
		}
	}

	class Child implements Demo{
		
		void fun(){
			
			System.out.println("Fun");
		}	
	
	}

	class Client {
		
		public static void main (String [] Args){
			
			Child obj = new Child();
			obj.fun();
			Demo.fun();
		}
	
	}
