

	interface Demo{
		
		static void fun(){
			
			System.out.println("In Fun ");
		}
	}

	class Child implements Demo{
	
	
	}

	class Client {
		
		public static void main (String [] Args){
			
			Child obj = new Child();
			obj.fun();
		}
	
	}
