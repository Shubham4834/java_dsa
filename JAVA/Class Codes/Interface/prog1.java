
	
	interface Demo{
		
		static void fun(){
		
			System.out.println("In Fun Demo");
		}

		default void gun(){
			
			System.out.println("In Fun Demo");
		}
	}
	
	class Child implements Demo{
		
		public void gun(){
			
			System.out.println("In Fun ChildDemo");
		}
	}

	class Client{

		public static void main (String [] Args){
		Child obj = new Child();
		obj.gun();
		
		
		} 
	}
