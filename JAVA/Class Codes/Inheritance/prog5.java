

	class Parent{
			int x=10;
			static int y=20;

			Parent(){
					System.out.println("In Parent Constructor");
			
			}

			void Access(){
				System.out.println("In Parent Access");
			
			}
	}

	class Child extends Parent{
	
			int x=100;
			static int y=200;

			Child(){
				System.out.println("In Child Constructor");
			
			}

			void Access(){

				super.Access();
				System.out.println(super.x);
				System.out.println(super.y);
				System.out.println(x);
				System.out.println(y);
			}
	
	}
		class Client{
		
		public static void main (String [] Args){
		
		Child obj = new Child();
		obj.Access();
		
		}
		
		
		
		}
