
	// Default Exception Handler 
	
class Demo{
	
	void m1(){
		
		System.out.println("Start void m1");
		System.out.println (10/0);
		m2();
	}

	void m2(){
		
		System.out.println("Start void m2");

	}
}

class Client {
	
	public static void main (String [] Args){
		
		Demo obj = new Demo();
		System.out.println("Start Main");
		
		obj.m1();

		System.out.println("End Main");
		
	}
}

	// EGxception in thread "Main ": java.lang.ArithmeticException : / by zero
//		at Demo.m1 (Prog3.java 12) 
//		at client.main(prog3.java 20) 	
