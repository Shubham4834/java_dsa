import java.util.*;

class Demo{
	
	void M1(int x){
		
		System.out.println("In M1");
		M2(x);
	}

	void M2(int x){
		
	try{
		System.out.println(10/x);

	} catch (ArithmeticException Ar){

		System.out.println(Ar.toString());
			
		System.out.println(Ar.getMessage());

		Ar.printStackTrace();
		
	} catch (NullPointerException NP){
	
		System.out.println(NP.toString());
		
	} catch ( NumberFormatException NF){
	
		System.out.println(NF.toString());
		
	} catch (IllegalArgumentException IA){
	
		System.out.println(IA.toString());
		
	} catch (Exception ex){
	
		System.out.println(ex.toString());
		
	}
}
}
class Client{

	public static void main (String [] Args){
	
		Scanner sc = new Scanner (System.in);
		int x = sc.nextInt();
		
		Demo obj = new Demo();
		obj.M1(x);
	}
}
