
import java.util.*;

class DataOverFlowException extends RuntimeException {
	 DataOverFlowException ( String msg){
			super (msg);
	}

}

class DataUnderFlowException extends RuntimeException {
	
	DataUnderFlowException( String msg){
		
		super (msg);
	}

}
class Client {
	
	public static void main ( String [] Args){
		
		int Arr[] = new int [5];

		Scanner sc = new Scanner (System.in);

		for ( int i=0 ; i< Arr.length ; i++){ 
			int data = sc.nextInt();

			if( data<0)
			       throw new DataUnderFlowException("Data is Less than 0 ");

			if (data > 100)
				throw new DataOverFlowException ("Data is Greater than 100 ");
		
			Arr[i]=data;

			} 

			for ( int i=0; i< Arr.length ; i++){
			
			System.out.print(Arr[i] + "  ");		
	}

}
}
