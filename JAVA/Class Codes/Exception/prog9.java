

	// User Define Exception (throw)
import java.util.*;

class Demo{
	
	public static void main (String [] Args){
		
		Scanner sc = new Scanner (System.in);
		
		int x = sc.nextInt();
		
		try{
			if (x==0)
				throw new ArithmeticException ("Exception Handled by Jd" );
			
			System.out.println(10/x);
		} catch ( ArithmeticException obj){
			
		System.out.print("Exception in Thread" + " " + Thread.currentThread().getName()+ " ");
		obj.printStackTrace();
		
		}
	}
}
