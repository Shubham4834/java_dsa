	class Demo{
	
		int x=10;
		Demo(){
			System.out.println("In No Args Constructor");
		
		}

		Demo(int x){

			this();
			System.out.println("In Para Constructor");
		}

		public static void main (String [] Args){
			Demo obj = new Demo(400);	
		
		}
	
	}
