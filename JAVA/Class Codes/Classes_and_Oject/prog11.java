
	class Demo{
	
		static{
		
			System.out.println("Static Block 1");
		}

		public static void main (String [] Args){
		
			
			System.out.println("In Main Demo");
		}
	}

	class Client {
	
		static{
			
			System.out.println("Static Block 2");
			
		}

		public static void main(String [] Args){
		
		 
		 	System.out.println("In Parent Main");
		}

		static{
			
			System.out.println("Static Block 3 ") ;
			
		}
	}
