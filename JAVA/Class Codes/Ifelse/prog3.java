import java.io.*;

class whiledemo {

	public static void main (String [] Args) throws IOException{

		BufferedReader br = new BufferedReader ( new InputStreamReader( System.in));
		System.out.println ("Enter the value of n");

		int n = Integer.parseInt(br.readLine());

		int i=1;
		int count= 0;

		while(i<=n){

			if(i%2 ==1){
				System.out.print( i + " ");
				count++;
			}
			i++;
			
		
		}

		System.out.println();
		System.out.println("The count is " +count);
		
	//	System.out.println();

	}
}

