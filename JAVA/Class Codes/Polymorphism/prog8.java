
	class Parent{
	
		void fun(int x){
			
			System.out.println("In Parent FUn");
		
		}
	
	}

	class Child extends Parent{
	
		void fun(){
		
			
			System.out.println("In Parent FUn");
		
		}
	
	}

	class Client{
	
		public static void main (String [] Args){
		
		Parent obj = new Child();
		obj.fun();
		
		} 
	
	
	}
