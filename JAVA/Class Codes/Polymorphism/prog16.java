

	class Parent{

		public void fun (){

			System.out.println("In Parent Fun");
		
		}
	
	}

	class Child extends Parent{
	
		void fun (){
		
			System.out.println("In Parent Fun");
		}
	}

	class Client{
	
	public static void main(String [] Args){

	Parent obj= new Child();
	obj.fun();
	
	}
	}
