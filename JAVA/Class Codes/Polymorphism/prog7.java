
	class Parent{
	
		void fun(){
			
			
			System.out.println("In Parent FUn");
		
		}
	}

	class Child extends Parent{
	
		void fun(int x){
		
			System.out.println("In Child FUn");
		}
	
	}

	class Client{
	
	public static void main(String [] Args){
	
		Child obj = new Child();
	       obj.fun();	
	
	
	} 
	
	}
