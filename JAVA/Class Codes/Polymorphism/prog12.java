

 class Demo{
 
 	void fun(String str1){
	
	System.out.println("In String");
	
	}

 	void fun(StringBuffer str1){
	
	System.out.println("In StringBuffer");
	
	}
 
 }

class Client{

	public static void main(String [] Args ){
	
	Demo obj = new Demo();
	obj.fun("Core2Web");
	obj.fun(new StringBuffer("C2W"));
	//obj.fun(null);

	}

}
