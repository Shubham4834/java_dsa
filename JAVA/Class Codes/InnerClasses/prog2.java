

class Outer{

	int x =10;
	static int y=20;
	
	class Inner{

		int a =30;
	      final static int b = 40;	
		
	}
}

class Client{
	
	public static void main (String [] Args){
	
		Outer obj = new Outer();
		Outer.Inner obj1= obj.new Inner();
	
	}

}
