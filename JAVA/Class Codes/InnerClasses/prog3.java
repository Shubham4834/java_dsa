

  // Annonymus Inner Class
  

class Demo{
	
	void marry(){
		
		System.out.println("Kriti Sanon");
	}
}

class Client{

	public static void main (String Args[]){
		
		Demo obj = new Demo(){
		
			void marry(){
				
				System.out.println("Disha Patani");
				fun();
			}
			
			void fun(){
				
				System.out.println("In Fun");
			}
		};
		
		obj.marry();
	}
}
