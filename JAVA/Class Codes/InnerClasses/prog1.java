

class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In Inner class");
		}
	
	}
	void m2(){
		
		System.out.println("In M2 outer");
	
	}

}

class Client{
	
	public static void main (String [] Args){
	
		Outer obj = new Outer();
		Outer.Inner obj1= obj.new Inner();

		obj1.m1();
		obj.m2();
	
	}

}
